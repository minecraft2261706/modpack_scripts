import lib
import argparse
import tempfile
import glob
import os
import shutil

def glob_escape_brackets(string):
    return string.replace("[", "[[]").replace("?","[?]")

def update_server(pack_toml, pack_src, instance_dir):
    overrides_dir = f"{pack_src}/overrides"
    shutil.rmtree(f"{instance_dir}/config", ignore_errors=True)
    shutil.rmtree(f"{instance_dir}/mods", ignore_errors=True)
    shutil.copytree(f"{overrides_dir}/config", f"{instance_dir}/config")
    shutil.copytree(f"{overrides_dir}/mods", f"{instance_dir}/mods")
    client_only_mods = pack_toml.get("client_only_mods", [])
    for client_only_mod in client_only_mods:
        client_only_mod_escaped = glob_escape_brackets(client_only_mod)
        for file in glob.glob(f"{instance_dir}/mods/{client_only_mod_escaped}"):
            print(f"Removing client-side mod: {file}")
            os.remove(file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("instance_dir")
    parser.add_argument("pack_zip_url")

    args = parser.parse_args()
    td = tempfile.mkdtemp()

    try:
        pack_toml, vers, pack_src = lib.extract_modpack(td, lib.download_modpack(td, args.pack_zip_url))

        update_server(pack_toml, pack_src, args.instance_dir)
        lib.apply_version_updates(pack_toml, vers, args.instance_dir)
    finally:
        shutil.rmtree(td)