import os
import shutil
import toml
import wx
import requests
import time
import threading
import atexit

import lib
import tempfile
import re
from pathlib import Path

td = ""
# coding 1000
destroy = False
destroyed = False
cv = threading.Condition()

def export_multimc(pack_src, dest_dir, name, pack_toml, vers):
    mmc_export = f"{pack_src}/mmc_export"
    if os.path.isdir(mmc_export):
        shutil.rmtree(mmc_export, ignore_errors=True)
    os.mkdir(mmc_export)
    mmc_export_mc = f"{mmc_export}/.minecraft"
    os.mkdir(mmc_export_mc)
    overrides_dir = f"{pack_src}/overrides"
    defaults_dir = f"{pack_src}/defaults"
    shutil.copytree(f"{overrides_dir}/config", f"{mmc_export_mc}/config")
    shutil.copytree(f"{overrides_dir}/mods", f"{mmc_export_mc}/mods")
    shutil.copy(f"{overrides_dir}/servers.dat", mmc_export_mc)
    shutil.copy(f"{overrides_dir}/{name}.png", mmc_export)
    shutil.copy(f"{overrides_dir}/mmc-pack.json", mmc_export)
    shutil.copy(f"{defaults_dir}/options.txt", mmc_export_mc)
    shutil.copy(f"{defaults_dir}/instance.cfg", mmc_export)
    lib.apply_version_updates(pack_toml, vers, mmc_export_mc)
    shutil.make_archive(dest_dir + "/" + name, "zip", mmc_export)

def update_multimc(pack_src, instance_dir, name, pack_toml, vers):
    mc_dir = f"{instance_dir}/.minecraft"
    overrides_dir = f"{pack_src}/overrides"
    shutil.rmtree(f"{mc_dir}/config", ignore_errors=True)
    shutil.rmtree(f"{mc_dir}/mods") # if instance is in use this will fail
    shutil.copytree(f"{overrides_dir}/config", f"{mc_dir}/config")
    shutil.copytree(f"{overrides_dir}/mods", f"{mc_dir}/mods")
    shutil.copy(f"{overrides_dir}/servers.dat", f"{mc_dir}/")
    shutil.copy(f"{overrides_dir}/mmc-pack.json", f"{instance_dir}/")

    icon_dir = f"{instance_dir}/../../icons"
    shutil.copy(f"{overrides_dir}/{name}.png", f"{icon_dir}/")
    instance_cfg = f"{instance_dir}/instance.cfg"
    with open(instance_cfg, "r") as sources:
        lines = sources.readlines()
    with open(instance_cfg, "w") as sources:
        for line in lines:
            sources.write(re.sub("^iconKey=.*$", f"iconKey={name}", line))
    lib.apply_version_updates(pack_toml, vers, instance_dir + "/.minecraft/")

OMU_OLD_PATH = str(Path.home())+"/omu.toml"
OMU_PATH = str(Path.home())+"/.omu.toml"

class MmcUpdater(wx.Frame):
   
    def __init__(self, parent, title):
        super(MmcUpdater, self).__init__(parent, title=title)
        global td
        td = ""
        self.pack_toml, self.vers, self.pack_src  = (None,None,None)
        self.in_progress = False

        self.SetSize(wx.Size(700,200))
        self.InitUI()
        self.LoadConfig()
        self.Centre()
        self.Show()
        

    
    def LoadConfig(self):
        if os.path.exists(OMU_OLD_PATH) and not os.path.exists(OMU_PATH):
            shutil.copy(OMU_OLD_PATH, OMU_PATH)
        try:
            with open(OMU_PATH, 'r') as f:
                self.conf = toml.load(f)
                self.modpack_url_tc.SetValue(value=self.conf["pack_url"] if "pack_url" in self.conf.keys() else "")
                self.instance_dir_ctrl.SetPath(self.conf["inst_dir"] if "inst_dir" in self.conf.keys() else "")
                self.mmc_dir_ctrl.SetPath(self.conf["dest_zip_dir"] if "dest_zip_dir" in self.conf.keys() else "")
        except Exception as e:
            print(e)
            self.conf = {"pack_url":None,"inst_dir":None,"dest_zip_dir":None}

    def InitUI(self):

        panel = wx.Panel(self)

        hbox = wx.BoxSizer(wx.HORIZONTAL)

        gbs = wx.GridBagSizer(5, 5)

        modpack_url = wx.StaticText(panel, label="Modpack URL:")
        self.modpack = wx.StaticText(panel, label="Modpack:")
        self.modpack.SetMinSize(wx.Size(200, 20))
        instance_directory = wx.StaticText(panel, label="Instance Directory:")
        dest_multimc_directory = wx.StaticText(panel, label="Destination MultiMC\n ZIP Directory:")

        self.modpack_url_tc = wx.TextCtrl(panel)
        self.modpack_url_tc.SetMinSize(wx.Size(250,20))
        self.Bind(wx.EVT_TEXT, self.HandleModpackUrlUpdated, self.modpack_url_tc)

        self.modpack_dl_gauge = wx.Gauge(panel, range=100, size=(250, -1))
        
        self.modpack_dl_prog = wx.StaticText(panel)
        self.modpack_dl_prog.SetFocus()
        
        self.download_btn = wx.Button(panel, label="Download")
        self.Bind(wx.EVT_BUTTON, self.HandleDownloadClicked, self.download_btn)
        
        self.instance_dir_ctrl = wx.DirPickerCtrl(panel)
        self.instance_dir_ctrl.SetPickerCtrlGrowable(True)
        self.mmc_dir_ctrl = wx.DirPickerCtrl(panel)
        self.mmc_dir_ctrl.SetPickerCtrlGrowable(True)
        update_instance = wx.Button(panel, label="Update Instance")
        self.Bind(wx.EVT_BUTTON, self.HandleUpdateInstance, update_instance)
        export_zip = wx.Button(panel, label="Export ZIP")
        self.Bind(wx.EVT_BUTTON, self.HandleExportZip, export_zip)

        gbs.Add(modpack_url, pos=(0,0))
        gbs.Add(self.modpack_url_tc, pos=(0,1), flag=wx.EXPAND)
        gbs.Add(self.download_btn, pos=(0,2))
        gbs.Add(self.modpack, pos=(1,0))
        gbs.Add(self.modpack_dl_gauge, pos=(1,1))
        gbs.Add(self.modpack_dl_prog, pos=(1,2))
        gbs.Add(instance_directory, pos=(2,0))
        gbs.Add(self.instance_dir_ctrl, pos=(2,1), span=(1,2), flag=wx.EXPAND)
        gbs.Add(update_instance, pos=(2,3), flag=wx.EXPAND)
        gbs.Add(dest_multimc_directory, pos=(3,0))
        gbs.Add(self.mmc_dir_ctrl, pos=(3,1), span=(1,2), flag=wx.EXPAND)
        gbs.Add(export_zip, pos=(3,3))

        hbox.Add(gbs, proportion=1, flag=wx.ALL|wx.EXPAND, border=15)
        panel.SetSizer(hbox)

        self.Bind(wx.EVT_CLOSE, self.on_close)

    def HandleDownload(self):
        # refund cs degree
        global destroyed
        try:
            pack_url = self.conf["pack_url"]
            r = requests.get(pack_url, stream=True, allow_redirects=True)
            assert r.status_code == 200, r.status_code
            block_size=1024
            elapsed, rate, d_last =(0, 0 , 0)
            t_last = time.time()
            self.modpack_dl_gauge.Show()
            self.modpack_dl_prog.Show()
            with open(f"{td}/pack.zip", "wb") as file:
                for data in r.iter_content(block_size):
                    if destroy:
                        break
                    elapsed+=len(data)
                    t_elapsed = time.time() - t_last
                    if t_elapsed>1:
                        rate = ((elapsed-d_last)/1024)/t_elapsed
                        d_last=elapsed
                        t_last = time.time()
                    data_f = f"{elapsed/1048576:.2f} Mb" if elapsed>1048576 else f"{elapsed/1024:.2f} Kb"
                    newval = (elapsed%10000000)//100000
                    self.modpack_dl_gauge.SetValue(newval)
                    self.modpack_dl_prog.SetLabel(f"{data_f} @ {rate:.2f} Kb/sec")
                    file.write(data)
            if not destroy:
                self.modpack_dl_gauge.SetValue(100)
                self.modpack_dl_prog.SetLabel("Complete!")

                zip_loc = f"{td}/pack.zip"
                self.pack_toml, self.vers, self.pack_src = lib.extract_modpack(td, zip_loc)
                wx.MessageBox(caption="Info", message="Successfully downloaded modpack!", style=wx.OK | wx.ICON_INFORMATION)
                modpackname = self.pack_toml.get("name", "(no name)")
                self.modpack.SetLabel(f"Modpack: {modpackname} {self.vers}")
        except Exception as e:
            wx.MessageBox(caption="Error", message=f"Error downloading modpack: {e}", style=wx.OK | wx.ICON_ERROR)
        finally:
            if not destroy:
                self.in_progress = False
                if self.modpack_url_tc.GetValue() != pack_url:
                    self.download_btn.Enable()
            with cv:
                destroyed = True
                cv.notify()

    def HandleDownloadClicked(self, e):
        global td
        print("Downloading modpack..")
        if td != "":
            shutil.rmtree(td, ignore_errors=True)
        
        td = tempfile.mkdtemp()
        self.pack_toml, self.vers, self.pack_src  = (None,None,None)
        self.modpack.SetLabelText(f"Modpack:")
        self.download_btn.Disable()
        self.in_progress = True
        try:
            self.conf["pack_url"]=self.modpack_url_tc.GetValue()
            with open(OMU_PATH, 'w') as f:
                toml.dump(self.conf, f)
            t = threading.Thread(target=self.HandleDownload)
            t.start()
        except Exception as e:
            wx.MessageBox(caption="Error", message=f"Error downloading modpack: {e}", style=wx.OK | wx.ICON_ERROR)
            self.download_btn.Enable()

    def HandleModpackUrlUpdated(self,e):
        if not self.in_progress:
            self.download_btn.Enable()

    def HandleUpdateInstance(self,e):
        if not self.pack_src:
            wx.MessageBox(caption="Error", message=(f"Download and extract a modpack first!"), style=wx.OK | wx.ICON_ERROR)
            return
        modpackname = self.pack_toml.get("name", "untitled_modpack")
        instance_dir = self.instance_dir_ctrl.GetPath()
        self.conf["inst_dir"] = instance_dir
        with open(OMU_PATH, 'w') as f:
            toml.dump(self.conf, f)
        try:
            update_multimc(self.pack_src, instance_dir, modpackname, self.pack_toml, self.vers)
            wx.MessageBox(caption="Info", message=f"Successfully updated MultIMC instance at {instance_dir}!", style=wx.OK | wx.ICON_INFORMATION)
        except Exception as e:
            wx.MessageBox(caption="Error", message=f"Error updating instance: {e}", style=wx.OK | wx.ICON_ERROR)

    def HandleExportZip(self, e):
        if not self.pack_src:
            wx.MessageBox(caption="Error", message=(f"Download and extract a modpack first!"), style=wx.OK | wx.ICON_ERROR)
            return
        modpackname = self.pack_toml.get("name", "untitled_modpack")
        dest_zip_dir = self.mmc_dir_ctrl.GetPath()
        dest_zip = dest_zip_dir + f"/{modpackname}.zip"
        self.conf["dest_zip_dir"] = dest_zip_dir
        with open(OMU_PATH, 'w') as f:
            toml.dump(self.conf, f)
        
        try:
            export_multimc(self.pack_src, dest_zip_dir, modpackname, self.pack_toml, self.vers)
            wx.MessageBox(caption="Info", message=f"Successfully exported MultIMC zip to {dest_zip}!", style=wx.OK | wx.ICON_INFORMATION)
        except Exception as e:
            wx.MessageBox(caption="Error", message=f"Error exporting modpack to zip: {e}", style=wx.OK | wx.ICON_ERROR)

    def on_close(self, event):
        global destroy
        global destroyed
        try:
            destroy = True
            if self.in_progress:
                with cv:
                    cv.wait_for(lambda: destroyed)
            cleanup_directories()
            # Close the frame
            self.Destroy()
        except Exception as e:
            print(f"Exception during application exit: {e}")
            raise
    

def cleanup_directories():
    if td != "":
        shutil.rmtree(td, ignore_errors=False)

def main():
    try:
        app = wx.App()
        ex = MmcUpdater(None, title='Overcomplicated Modpack Updater')
        ex.Show()
        ex.modpack_dl_gauge.Hide()
        ex.modpack_dl_prog.Hide()
        app.MainLoop()
    except Exception as e:
        print(e)
    finally:
        del ex

import sys
if __name__ == "__main__":
    main()
    sys.exit(0)